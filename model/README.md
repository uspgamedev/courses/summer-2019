
# Módulo da arquitetura experimental

Aqui vou fazer ume breve descrição da API do módulo

## Stage

O protótipo `Stage` em `model/stage.lua` contém todas as entidades do modelo/simulação do jogo, e é
responsável por sua criação, remoção, composição e consulta.

### :create()

Cria uma entidade nova e coloca ela no cenário.

```lua
local id = stage:create(entity_name)
```

A entidade é criada segundo o arquivo encontrado em
`database/entities/<entity_name>.lua`, segundo este formato:

```lua
-- database/entities/example.lua
return {
  -- Usado para definir o ID da entidade
  name = "Example",
  properties = {
    -- Aqui as chaves são os *typenames* das propriedades
    body = {
      speed = 200,
      weight = 50
    },
    damageable = {
      max_hp = 100,
      resistence = 20
    }
  }
}
```

Onde cada propriedade será instanciada usando

```lua
local property = new 'model.properties.<typename>' { ... }
```

Com os atributos fornecidos pela tabela correspondente no arquivo do banco de
dados. No exemplo acima, `stage:create()` vai fazer mais ou menos o equivalente
a:

```lua
local entity_data = require 'database.entities.example'
-- Cria propriedade body
local body_data = entity_data.properties.body
local body = new 'model.property.body' (body_data)
-- Cria propriedade damageable
local damageable_data = entity_data.properties.damageable
local damageable = new 'model.property.damageable' (damageable_data)
```

O id devolvido por `stage:create()` é o identificador da entidade criada. Ela
vai conter todas as propriedades descritas no seu registro no banco de dados.
Você pode usar esse id para consultar as propriedades da entidade através do
método `stage:find(id)`.

### :add()

Adiciona uma propriedade a uma entidade (possivelmente criando ela).

```lua
stage:add(id, property)
```

O objeto propriedade já deve estar devidamente instanciado. Isso normalmente é
usado quando uma propriedade precisa ser criada manualmente no código e
adicionada a uma entidade em tempo de execução. Por exemplo, se uma torre fica
infectada por um zumbi e agora tem uma propriedade `explodable`.

### :find()

Consulta a propriedade do tipo específicado na entidade com o ID procidenciado.
Caso ela não tenha tal propriedade, devolve nulo.

```lua
local property = stage:find(id, typename)
```

### :remove()

Remove a entidade com este ID do cenário. Ou seja, remove todas suas propriedades.

```lua
stage:remove(id)
```

### :updateDomains()

Chama `onUpdate(dt)` em todas as propriedades de todos os tipos.

```lua
stage:updateDomains(dt)
```

## Property

O protótipo `Property` em `model/property/init.lua` é uma classe abstrata que
você herda para definir as características das entidades no jogo. Por exemplo,
você pode criar uma propriedade `damageable` que possui vida máxima e pode
receber dano, e assim toda entidade que tiver essa propriedade será vulnerável
a dano e passível de ser destruída.

Uma vez registradas (veja [stage:add()](#add)), as propriedades ganham uma
referência para o cenário atual (`property.stage`), assim como um identificador
(`property.id`, em geral uma string do tipo `"Hero#23"`).

### :onRegister()

Método de *callback* que é automaticamente chamado quando a propriedade é
adicionada a um cenário.

### :onRemove()

Método de *callback* que é automaticamente chamado quando a propriedade é
removida de um cenário, seja por si só ou porque a entidade inteira foi
removida.

### :onUpdate(dt)

Método de *callback* que é automaticamente chamado se a propriedade está em um
cenário e alguém executou `stage:updateDomains()`.

### :destroy()

Marca a propriedade para ser removida do cenário quando alguém executar
`stage:updateDomains()`

### :as(typename)

Procura e devolve a propriedade do tipo pedida que seja da mesma entidade. Por
exemplo, se uma entidade tem propriedades `body` e `damageable`, é possível
acessar qualquer uma a partir da outra:

```lua
local body = getSomeEntityBody()
-- Agora acessa a propriedade 'damageable'
local damageable = body:as('damageable')
```

Este método é o equivalente a fazer:

```lua
property.stage:find(property.id, typename)
```

