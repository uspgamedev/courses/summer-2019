
local Routine = new(Object) {
  stage = nil,
  log = nil
}

function Routine:getStatus()
  return 'ready'
end

function Routine:play(...)
  -- abstract
end

return Routine

