
require 'common'

local _world
local _player
local _entity_timer

function love.load()
  local W, H = love.graphics.getDimensions()
  _world = new 'world' {}
  _player = _world:makeEntity('player', new(Vec) { W/2, 9*H/10 }, -90)
  _entity_timer = 0
end

local CONTROLS = {
  up = new(Vec) { 0, -1 },
  down = new(Vec) { 0, 1 },
  left = new(Vec) { -1, 0 },
  right = new(Vec) { 1, 0 },
}

function love.update(dt)
  -- Update entity timer
  _entity_timer = _entity_timer + dt
  if _entity_timer >= 2 then
    -- Reseta contador
    _entity_timer = 0
    -- Cria entidade nova
    local W, H = love.graphics.getDimensions()
    local start_pos = new(Vec) { love.math.random() * W, -20 }
    local end_pos = new(Vec) { (2 * love.math.random() - 0.5) * W, H + 100 }
    _world:makeEntity('asteroid', start_pos, 90, end_pos - start_pos)
  end
  -- Controla o jogador
  local movement = new(Vec) {}
  for key,control_movement in pairs(CONTROLS) do
    if love.keyboard.isDown(key) then
      movement:translate(control_movement)
    end
  end
  _player:setMovement(movement)
  -- Atualiza estado do mundo
  _world:update(dt)
end

function love.draw()
  _world:draw()
end

