
return new 'spec' {
  speed = 300,
  firerate = 10,
  bullet = 'simple-bullet',
  view = {
    color = { .7, .2, .7 },
    type = 'polygon',
    params = { 'fill',  -10, 5, 10, 5, 0, -15 }
  }
}

