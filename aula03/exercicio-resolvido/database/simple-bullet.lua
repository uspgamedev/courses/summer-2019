
return new 'spec' {
  speed = 600,
  firerate = 0,
  bullet = nil,
  view = {
    color = { 1, 1, 1 },
    type = 'rectangle',
    params = { 'fill',  -4, -4, 8, 8 }
  }
}

