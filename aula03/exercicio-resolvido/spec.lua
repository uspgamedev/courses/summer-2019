
local Spec = new(Object) {
  speed = 100,
  firerate = 0,
  bullet = nil,
  view = {
    color = { 1, 1, 1 },
    type = 'rectangle',
    params = { 'fill', -5, -5, 10, 10 }
  }
}

return Spec

