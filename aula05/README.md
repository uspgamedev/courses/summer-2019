
# Aula 5

Nessa aula, vamos prosseguir com o exercício das aulas anteriores. Estamos desenvolvendo um jogo 2D shooter bem simples, que
pode ser visto e testado na pasta `gabarito` desta aula.

### Instruções

Para fazer os passos dessa aula **VOCÊ PRECISA TER FEITO *TODOS* OS PASSOS DA QUARTA AULA**.

Além disso, lembre-se de **nunca pular passos**. A sequência do exercício foi projetada para construir novas mecânicas em cima das mudanças anteriores, então as instruções ficam completamente sem sentido se seu código não tiver seguido todos os passos até aquele ponto.

Uma recomendação para ajudar a assimilar a sintaxe e o quê cada passo significa: **escrevam vocês mesmos o código indicado nos passos, não copiem!**

Por fim, tente entender o que cada passo está fazendo:

+ Por que estamos fazendo esse passo? Qual o objetivo dele?
+ O que esse trecho de código faz? Como ele contribui para o objetivo?
+ Como esse passo se encaixa no jogo como um todo? Onde eu coloco ele?

### Conceitos que vamos trabalhar

Na última aula nós analisamos os problemas de se ter **código repetido**. Nesta aula, nós vamos entender e aplicar os conceitos de **coesão** e **acoplamento** no desenvolvimento de jogos.

### Índice

[4. Destruição](#4-destruição)

[5. Colisão](#5-colisão)

[6. Mundo](#6-tiros-mundo)

## 4. Destruição

Vamos eliminar toda entidade que sair para muito longe da tela!

### 4.1 Detectando entidades fora da tela

+ Vamos usar objetos `Box` da [biblioteca auxiliar](aula05/gabarito/common/README.md).

```lua
local box = new(Box) { left, right, top, bottom }
local point = new(Vec) { x, y }

if box:isInside(point) then
  print("O ponto está dentro da caixa!")
end
```

+ Saber se um ponto está dentro ou não de uma caixa

```lua
-- main.lua
local _limits_box
function love.load()
  -- ...
  _limits_box = new(Box) { -100, W + 100, -100, H + 100 }
end

function love.update(dt)
  -- ...
  -- Fazer um novo for para ver quem está fora da tela
  for i, entity in ipairs(_entities) do
    if not _limits_box:isInside(entity.pos) then
      -- Detrói entidade
    end
  end
end
```

### 4.2 Retirando entidades da sequência

+ `table.remove(table, index)`

```lua
table.remove(_entities, i)
```

Isso não functiona, porque o `for` acaba pulando a próxima entidade!

![não faça isso](http://gameprogrammingpatterns.com/images/update-method-remove.png)

+ Como tirar elementos da sequência de maneira segura?

Você marca quais índices contêm entidades a serem removidas! Para isso, fazemos um dicionário que marca os índices das entidades a serem destruídas:

```lua
-- main.lua / love.update(dt)
local removed = {}
```

E marcamos as entidades que detectamos estarem fora da tela:


```lua
-- main.lua / love.update(dt)
-- Na hora de destruir a entidade, você só marca ela:
removed[i] = true
```

Aí, no final do `love.update(dt)`, a gente destrói as entidades marcadas de trás para frente, evitando problemas:

```lua
-- main.lua
function love.update(dt)
  -- ...
  for i=#_entities,1,-1 do
    if removed[i] then
      table.remove(_entities, i)
    end
  end
end
```

Você também podia iterar nas entidades de trás para frente, mas esse jeito vai ser mais vantajoso para o passo 5.

### 4.3 Refatoração

+ Fazer função `_updateEntities(dt)`

O `love.update(dt)` está ficando grande, não? Separe a parte dele que cuida de mover e remover entidades em uma função separada:

```lua
-- main.lua
local function _updateEntities(dt)
  -- move e remove entidades
end

function love.update(dt)
  -- ...
  _updateEntities(dt)
end
```

## 5. Colisão

Agora queremos que quaisquer duas entidades que colidam entre si sejam destruídas!

### 5.1 Como saber se duas entidades estão colidindo?

+ Cada entidade tem uma hitbox

```lua
-- entity.lua
function Entity:init()
  -- ...
  self.hitbox = self.hitbox or new(Box) { -5, 5, -5, 5 }
end
```

Lembre de atualizar no `_makeEntity(typename, pos, movment)` para `player` e para `enemy`! A hitbox do jogador vai ser `new(Box) { -4, 4, -4, 4 }` e a dos inimigos `new(Box) { -8, 8, -8, 8}`.

+ Criar método `Entity:collisdesWith(other)`

```lua
-- entity.lua
function Entity:collidesWith(other)
  return (self.hitbox + self.pos):intersects(other.hitbox + other.pos)
end
```

### 5.2 Como saber *quem* colidiu?

+ Verificamos todas contra todas (ou não)

| Colide? | e1 | e2 | e3 |
| --- | --- | --- | --- |
| e1 | / | Sim | Não |
| e2 | Sim | / | Sim |
| e3 | Não | Sim | / |

Note que (1) a diagonal da tabela é irrelevante e (2) ela é simétrica.

```lua
-- main.lua
local function _updateEntities(dt)
  -- ...
  local n = #_entities
  for i = 1,n do
    local entity1 = _entities[i]
    for j = i+1,n do
      local entity2 = _entities[j]
      if entity1:collidesWith(entity2) then
        -- Destrúa as duas!
      end
    end
  end
end
```

### 5.3 Destruindo

+ Aproveitar o código de destruição de entidades do passo [4.2](#retirando-entidades-da-sequencia)

É só colocar as entidades em colisão naquele dicionário `removed` e deixar o código que já existe fazer o resto do trabalho:

```lua
removed[i] = true
removed[j] = true
```

## 6. ~~Tiros~~ Mundo

Se a gente tentasse fazer entidades atirarem agora, íamos ter um problema. Para uma entidade atirar, ela precisa poder modificar o estado não só dela, mas da simulação do jogo como um todo. Mais especificamente, ela precisa poder alterar a lista de entidades. Então, antes de fazer tiros, vamos refatorar o código de simulação do jogo em um objeto `World` que contém todas as entidades ativas do jogo, e vamos deixar cada entidade ter uma referência para ele para poder alterar seu estado.

### 6.1 Admirável mundo novo

+ Criar módulo `world.lua`

```lua
-- world.lua
local World = new(Object) {
  entities = nil -- não pode inicializar aqui!
}

function World:init()
  self.entities = self.entities or {}
end

return World
```

+ Mover lista de entidades para lá

```lua
-- main.lua
local _world

function love.load()
  _world = new 'world' {}
  -- ...
end
```

Além disso, temos que trocar todos os usos de `_entities` por `_world.entities`. Por hora.

Fique testando o jogo até ele voltar a funcionar como antes! Só então siga para o próximo passo.

### 6.2 O êxodo

Agora nós vamos pegar o codigo que manipula o mundo e transferir para métodos no protótipo `World`.

+ `World:makeEntity(typename, pos, movement)`

É só mudar o `_makeEntity(typename, pos, movement)` do `main.lua` para `world.lua`, arrumando a declaração dele para:

```lua
-- world.lua
function World:makeEntity(typename, pos, movement)
  -- cria uma entidade nova
end
```

E trocando todos os `_world.entities` por `self.entities`.

Além disso, todos os lugares que chamavam `_makeEntity(...)` agora têm que chamar `_world:makeEntity(...)`.

+ `World:draw()`

Mova todo o código do `love.draw()` para um novo método no protótipo `World`:

```lua
-- world.lua
function World:draw()
  -- desenha entidades
end
```

Troque todos os usos de `_world.entities` para `self.entities`, e arrume o `main.lua` para chamar esse novo método:

```lua
-- main.lua
function love.draw()
  _world:draw()
end
```

+ `World:update(dt)`

Aqui nós vamos mover só parte do código do `love.udpate(dt)`. Nós NÃO vamos mover o código de controlar o jogador nem de criar inimigos com o timer. Então o código na `main.lua` vai ficar mais ou menos assim:

```lua
function love.update(dt)
  -- Controla jogador
  -- Atualiza timer e cria mais inimigos se for o caso
  _world:update(dt)
end
```

O resto do código (atualizar estado das entidades, verificar entidades fora da tela, verificar entidades colidindo e destruir entidades) fica no novo método no protótipo `World`:

```lua
-- world.lua
function World:update(dt)
  -- atualiza estado das entidades
  -- verifica entidades fora da tela
  -- verifica entidades em colisão
  -- destrói entidades
end
```

Mais uam vez, lembre-se de mudar todos os `_world.entities` para `self.entities`.

### 6.3 Entidades conhecem o mundo

+ Injetar referência para `World` na criação de entidades

Agora que temos um objeto para representar o estado do mundo (ou universo?) sendo simulado no jogo, podemos fazer toda entidade criada ter uma referência para esse objeto, o que permite ela intervir no estado de outras entidades e outras partes do mundo. Para isso, basta acrescentar uma linha em `World:makeEntity(...)`:

```lua
-- world.lua
function World:makeEntity(typename, pos, movement)
  -- ...
  new_entity.world = self
  -- ...
end
```

+ Note que isso seria bem mais difícil se a criação não estivesse centralizada!

Você teria que mudar *todas as partes do código que criam entidades*. Graças à `World:makeEntity(...)`, isso não será necessário. Vamos estudar isso em outra aula, mas dizemos que nós *desacoplamos a criação de entidades do uso de entidades*. Isto é, quem usa objetos de entidade não precisa mais saber como entidades são criadas e, portanto, podemos mudar como entidades são criadas sem afetar quem as usa! Se ficou confuso, tudo bem, vamos ver isso de novo depois.

## Continuação

[Na aula 6!](aula06)

