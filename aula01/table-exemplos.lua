
local t = {}

local sequence = {1, 2, 3}

print(sequence[2], sequence[4])

for index,value in ipairs(sequence) do
  print(index, value)
end

print()

local dict = { a = 42, b = 1337 }

print(dict['a'], dict['foo'])

for key,value in pairs(dict) do
  print(key, value)
end

print()

for k,v in pairs({ 1, 2, x = 32, 3}) do
  print(k,v)
end

print()

local obj = {}

obj.x = 42 -- obj['x'] = 42

print(obj.x, obj.y)

function obj:foo(a)
  print(self.x + a)
end

print(obj:foo(3))

print "hey there"

function foo(t)
  print(t.x)
end

foo { x = 53 }

--[[

Saída esperada:

2	nil
1	1
2	2
3	3

42	nil
b	1337
a	42

1	1
2	2
3	3
x	32

42	nil
45

hey there
53

]]
