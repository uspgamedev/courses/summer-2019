
local i

--[[
-- Isto é
-- um comentário
-- multi-linha
--]]

function love.load(args)
  i = tonumber(args[1] or 5)
end

function love.update(dt)
  i = i - dt
  if i <= 0 then
    love.event.quit()
  end
end

function love.draw()
  love.graphics.print("time: " .. i, 100, 100)
  love.graphics.polygon('fill', 300+i*10, 234, 206, 457, 675, 120)
end

