


local x = 42


if x < 10 then
  print("menor")
elseif x == 10 then
  print("igual")
else
  print("maior")
end

print()

local i = 1
while i < 10 do
  print(i)
  i = i + 1
end

print()

for i=1,10 do
  print(i)
end

print()

for i=10,1,-3 do
  print(i)
end

print()

local i = 1
repeat
  print(i)
  i = i + 1
  if i % 5 == 0 then break end
until i == 10

--[[
  
Saída esperada:

maior

1
2
3
4
5
6
7
8
9

1
2
3
4
5
6
7
8
9
10

10
7
4
1

1
2
3
4

]]
