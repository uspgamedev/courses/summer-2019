
function foo(a,b)
  return a + b
end

function test()
  x = foo(1,2)
  print(x)

  x = foo(1,2,3)
  print(x)

  x = foo(1) -- error?
  print(x)
end

function foo(a, b)
  a = a or 0
  b = b or 0
  return a + b
end

print(1, 2, 3, 4)

print("x = " .. 42)

if not pcall(test) then
  print("test failed")
end

print()

function bar(x)
  return x, x+1, x+2
end

y,z,w,s = bar(1)
print(y,z,w,s)

--[[

Saída esperada:

1	2	3	4
x = 42
3
3
1

1	2	3	nil

]]


