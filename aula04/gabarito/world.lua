
local World = new(Object) {}

function World:init()
  self.entities = {}
  local W, H = love.graphics.getDimensions()
  self.hitbox = new(Box) { -100, W + 100, -100, H + 100 }
end

function World:makeEntity(specname, pos, dir, movement)
  local new_entity = new 'entity' {
    spec = new 'spec' (require('database.' .. specname)),
    pos = new(Vec) { pos:get() },
    dir = dir or 0,
    world = self
  }
  if movement then
    new_entity:setMovement(movement)
  end
  table.insert(self.entities, new_entity)
  return new_entity
end

function World:update(dt)
  -- Atualiza estado de todas entidades
  for _,entity in ipairs(self.entities) do
    entity:update(dt)
  end
  -- Verifica entidades que morreram
  local removed = {}
  -- (1) por colisão
  local n = #self.entities
  for i = 1,n do
    local entityA = self.entities[i]
    for j = i+1,n do
      local entityB = self.entities[j]
      if entityA:collidesWith(entityB) then
        removed[i] = true
        removed[j] = true
      end
    end
  end
  -- (2) por sair da tela
  for i,entity in ipairs(self.entities) do
    if not self.hitbox:isInside(entity.pos) then
      removed[i] = true
    end
  end
  -- Agora remove de fato as entidades
  for i = #self.entities,1,-1 do
    if removed[i] then
      table.remove(self.entities, i)
    end
  end
end

function World:draw()
  for _,entity in ipairs(self.entities) do
    entity:draw()
  end
end

return World

