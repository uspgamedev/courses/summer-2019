
return {
  name = "Simple Bullet",
  speed = 600,
  hitbox = { -4, 4, -4, 4 },
  view = {
    color = { 1, 1, 1 },
    type = 'rectangle',
    params = { 'fill',  -4, -4, 8, 8 }
  }
}

