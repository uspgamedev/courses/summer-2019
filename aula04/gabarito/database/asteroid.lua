
return {
  name = "Asteroid",
  speed = 200,
  hitbox = { -8, 8, -8, 8 },
  view = {
    color = { 0.9, 0.1, 0.1 },
    type = 'rectangle',
    params = { 'fill',  -8, -8, 16, 16 }
  }
}

